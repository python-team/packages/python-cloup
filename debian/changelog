python-cloup (3.0.6-1) unstable; urgency=medium

  * New upstream version 3.0.6
  * Update patches
    - Drop 0003-click-8.2.patch (merged by upstream)
  * Wrap and sort Debian package files
  * Bump Standards-Version to 4.7.2

 -- Timo Röhling <roehling@debian.org>  Thu, 06 Mar 2025 20:23:31 +0100

python-cloup (3.0.5-2) unstable; urgency=medium

  * Team upload.
  * Add the 0003-click-8.2 patch to fix `make_metavar()`.
  * Add Salsa CI configuration with reprotest temporarily disabled
    (figure out how to fix the sphinx docs generation if possible).

 -- Peter Pentchev <roam@debian.org>  Thu, 20 Feb 2025 15:39:55 +0200

python-cloup (3.0.5-1) unstable; urgency=medium

  * New upstream version 3.0.5

 -- Timo Röhling <roehling@debian.org>  Thu, 07 Mar 2024 16:02:47 +0100

python-cloup (3.0.4-1) unstable; urgency=medium

  * New upstream version 3.0.4

 -- Timo Röhling <roehling@debian.org>  Tue, 02 Jan 2024 21:04:17 +0100

python-cloup (3.0.3-2) unstable; urgency=medium

  * Prevent sphinx-build from accessing the network (Closes: #1058062)

 -- Timo Röhling <roehling@debian.org>  Mon, 11 Dec 2023 21:42:27 +0100

python-cloup (3.0.3-1) unstable; urgency=medium

  * New upstream version 3.0.3

 -- Timo Röhling <roehling@debian.org>  Fri, 17 Nov 2023 21:22:30 +0100

python-cloup (3.0.2-1) unstable; urgency=medium

  * New upstream version 3.0.2

 -- Timo Röhling <roehling@debian.org>  Fri, 08 Sep 2023 22:58:03 +0200

python-cloup (3.0.1-1) unstable; urgency=medium

  * New upstream version 3.0.1

 -- Timo Röhling <roehling@debian.org>  Mon, 28 Aug 2023 17:58:42 +0200

python-cloup (3.0.0-1) unstable; urgency=medium

  * New upstream version 3.0.0
  * Use Furo theme for documentation

 -- Timo Röhling <roehling@debian.org>  Mon, 17 Jul 2023 00:23:27 +0200

python-cloup (2.1.1-1) unstable; urgency=medium

  * New upstream version 2.1.1
  * Bump Standards-Version to 4.6.2

 -- Timo Röhling <roehling@debian.org>  Tue, 13 Jun 2023 23:48:13 +0200

python-cloup (2.0.0.post1-2) unstable; urgency=medium

  * Use the shiny new pybuild-autopkgtest

 -- Timo Röhling <roehling@debian.org>  Thu, 08 Dec 2022 13:18:30 +0100

python-cloup (2.0.0.post1-1) unstable; urgency=medium

  * New upstream version 2.0.0.post1

 -- Timo Röhling <roehling@debian.org>  Mon, 14 Nov 2022 17:43:31 +0100

python-cloup (1.0.2-1) unstable; urgency=medium

  * New upstream version 1.0.2

 -- Timo Röhling <roehling@debian.org>  Sun, 06 Nov 2022 19:31:18 +0100

python-cloup (1.0.1-3) unstable; urgency=medium

  [ Debian Janitor ]
  * Set upstream metadata fields: Repository-Browse.
  * Set upstream metadata fields: Repository.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Sun, 16 Oct 2022 22:28:23 +0100

python-cloup (1.0.1-2) unstable; urgency=medium

  [ Debian Janitor ]
  * Set field Upstream-Name in debian/copyright.
  * Set upstream metadata fields: Bug-Database, Bug-Submit.

 -- Timo Röhling <roehling@debian.org>  Fri, 23 Sep 2022 20:54:44 +0200

python-cloup (1.0.1-1) unstable; urgency=medium

  * New upstream version 1.0.1

 -- Timo Röhling <roehling@debian.org>  Fri, 23 Sep 2022 20:52:13 +0200

python-cloup (1.0.0-2) unstable; urgency=medium

  * Fix d/clean

 -- Timo Röhling <roehling@debian.org>  Mon, 25 Jul 2022 18:00:44 +0200

python-cloup (1.0.0-1) unstable; urgency=medium

  * Clean SCM version file after build (Closes: #1015045)
  * New upstream version 1.0.0

 -- Timo Röhling <roehling@debian.org>  Mon, 25 Jul 2022 16:20:41 +0200

python-cloup (0.15.1-1) unstable; urgency=medium

  * New upstream version 0.15.1

 -- Timo Röhling <roehling@debian.org>  Tue, 28 Jun 2022 14:09:22 +0200

python-cloup (0.15.0-1) unstable; urgency=medium

  * New upstream version 0.15.0

 -- Timo Röhling <roehling@debian.org>  Wed, 22 Jun 2022 23:17:21 +0200

python-cloup (0.14.0-1) unstable; urgency=medium

  * Initial release (Closes: #1012687)

 -- Timo Röhling <roehling@debian.org>  Sat, 11 Jun 2022 22:17:29 +0200
